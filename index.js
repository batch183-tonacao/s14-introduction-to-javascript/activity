// console.log("Hello World");

let firstName = "John", lastName = "Smith", age="30";
let isMarried = false;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let workAddress = {
	houseNumber: "32",
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}

console.log("First Name: "+ firstName);
console.log("Last Name: "+ lastName);
console.log("Age: "+ age);
console.log("Hobbies: ");
console.log(hobbies);
console.log("Work Address: ");
console.log(workAddress);

function printUserInfo(firstName,lastName,age, hobbies, workAddress, isMarried){
	console.log(firstName+' '+lastName+" is "+ age +" years old.");
	console.log("This was printed inside of a function.");
	console.log(hobbies);
	console.log("This was printed inside of a function.");
	console.log(workAddress);
}
printUserInfo("John","Smith","30", hobbies, workAddress);

function status(isMarried){
	return "The value of isMarried: "+ isMarried;
}
let theStatus = status(true);
console.log(theStatus);

